sobre o projeto:
 - Api Restfull em Laravel5.5 - sem interface gráfica web, apenas endpoints para consumo via "postman"
 - banco de dados MySql

passo a passo

1 - crie um schema com base no dump que esta na raiz do projeto com o nome: "__db_leroy_products.sql";
2 - com o console/terminal chegue na pasta do projeto e rode o comando "composer update" (é necessário ter o composer instalado)
3 - após o fim do processo anterior ainda no console rode o comando "php artisan serv" provavelmente ira aparecer algo como: "http://localhost:8000/"
4 - abra o postman (caso não tenha instalado, instale por favor) e importe um dos dois arquivos "__teste_leroy.postman_collection1.json" ou "__teste_leroy.postman_collection1.json", coloquei duas opções apenas por eventual problema de compatibilidade com versão da ferramente utilizada.
5 - ajuste o arquivo excel do teste, deletando as duas primeiras linhas que estão vazias.
6 - comece a utilizar os endpoints pelo postman inicialmente pelo primeiro "localhost:8000/api/import-excel"

Estou a disposição para ajudar pelo e-mail: volverine.felix@gmail.com, ou o telefone 97170-8628
