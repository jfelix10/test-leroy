-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: db_leroy
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lm` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `free_shipping` tinyint(1) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,112,'furadeira Alterada','Ferramentas',1,'Furadeira super eficiente Y',1212.11,NULL,'2017-11-09 19:49:31'),(3,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(4,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(5,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(6,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(7,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(8,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(9,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(10,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(11,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(12,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(13,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(14,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(15,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(16,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(17,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(18,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(19,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(20,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(21,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(22,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(23,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(24,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(25,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(26,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(27,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(28,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(29,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(30,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(31,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(32,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(33,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(34,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(35,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(36,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(37,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(38,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(39,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(40,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(41,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(42,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(43,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(44,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(45,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(46,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(47,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(48,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL),(49,1001,'Furadeira X','Ferramentas',0,'Furadeira eficiente X',100.00,NULL,NULL),(50,1002,'Furadeira Y','Ferramentas',1,'Furadeira super eficiente Y',140.00,NULL,NULL),(51,1003,'Chave de Fenda X','Ferramentas',0,'Chave de fenda simples',20.00,NULL,NULL),(52,1008,'Serra de Marmore','Ferramentas',1,'Serra com 1400W modelo 4100NH2Z-127V-L',399.00,NULL,NULL),(53,1009,'Broca Z','Ferramentas',0,'Broca simples',3.90,NULL,NULL),(54,1010,'Luvas de Proteção','Ferramentas',0,'Luva de proteção básica',5.60,NULL,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-09 16:41:39
