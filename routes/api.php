<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('import-excel',[
    'uses'=>'FileController@importFile'
]);

Route::get('get-products',[
    'uses'=>'FileController@getProducts'
]);

Route::put('alter-products/{id}',[
    'uses'=>'FileController@putAlterProducts'
]);

Route::delete('delete-products/{id}',[
    'uses'=>'FileController@deleteProducts'
]);
