<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;

class FileController extends Controller
{
    private $products;
    public function __construct()
    {
        $this->products = new Products();
    }

    public function importFile(Request $request)
    {
        if(!is_null($request->file))
        {
            $path = $request->file->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count())
            {
                foreach ($data as $value)
                {
                    foreach ($value as $val)
                    {
                        $arr[] = [
                            'lm' => $val->lm
                            , 'name' => $val->name
                            , 'category' => $val->category
                            , 'free_shipping' => $val->free_shipping
                            , 'description' => $val->description
                            , 'price' => $val->price
                        ];
                    }
                }
                if(!empty($arr))
                {
                    $this->products->insert($arr);
                    return response()->json(['New products inserted Success!'], 201);
                }
            }
        }
        else if(is_null($request->file))
        {
            return response()->json(['please insert one file.'], 404);
        }
        else
        {
            return response()->json(['something is wrong, contact support!'], 404);
        }
    }

    public function getProducts()
    {
        $getAll = $this->products->all();
        $response = [
            'products' => $getAll
        ];

        return response()->json($response, 200);
    }

    public function putAlterProducts(Request $request, $id)
    {
        $product = $this->products->find($id);

        if (!$product)
        {
            return response()->json(['product not finded'], 404);
        }
        else
        {
            foreach ($request->input() as $key => $value)
            {
                // print_r($key);
                // print_r($value); die;
                $product->$key = $value;
            }
            $product->save();
            return response()->json(['product updated success!'], 200);
        }
    }

    public function deleteProducts($id)
    {
        $product = $this->products->find($id);
        if (!$product)
        {
            return response()->json(['message' => 'product not finded'], 404);
        }
        else
        {
            $product->delete();
            return response()->json(['product deleted'], 200);
        }
    }
}
