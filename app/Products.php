<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
      protected $fillable = [
          'lm', 'name', 'category', 'free_shipping', 'description', 'price'
      ];
}
